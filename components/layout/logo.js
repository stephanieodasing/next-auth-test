import classes from './logo.module.css';

function Logo() {
    return(
        <div className={classes.logo}>
           MINT09
        </div>
    )
};

export default Logo;