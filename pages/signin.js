
import { useState, useRef, useEffect } from 'react';
import { getProviders, useSession, getSession, signIn } from 'next-auth/client';
import Link from 'next/link';
import { useRouter} from 'next/router';
import { toast } from "react-toastify";

export default function Signin(props) {
  const router = useRouter();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // const [loginError, setLoginError] = useState('');
    const handleLogin = (event) => {
        event.preventDefault();
        event.stopPropagation();

        signIn("credentials", {
            email, password, callbackUrl: `${window.location.origin}/dashboard`, redirect: false }
        ).then(function(result){
            if (result.error !== null)
            {
                if (result.status === 401)
                {
                    // setLoginError("Your email/password combination was incorrect. Please try again");
                    toast.error("Your email/password is incorrect.");
                }
                else
                {
                    // setLoginError(result.error);
                    toast.error(result.error)
                }
            }
            else
            {
                router.push(result.url);
            }
        });

      
        
    }
    
    return (
      <>
      <form onSubmit={handleLogin}>
        <br/>
        <label>
          Email: <input type='text' name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
        </label>
        <label>
          Password: <input type='password' name="password" value={password} onChange={(e) => setPassword(e.target.value)} />
        </label>
          <button type='submit'>Submit login</button>
          <br/>
          <Link href="/signup">
          <a> Don't have an account?  <b>Signup</b></a>
          </Link>
          <button
              onClick={() =>
                signIn("google", { callbackUrl: "http://localhost:3000/dashboard" })
              }
            >
              Sign In with Google
            </button>
      </form>

      <div className="">
      {Object.values(props.providers).map((provider) => {
          if(provider.name === 'Credentials') {
              return;
          }
          return (
              <div key={provider.name}>
                  <button className="" onClick={() => signIn(provider.id)}>{provider.name}</button>
              </div>
          )
      })}
      </div>
</>
    )
  }


  export async function getServerSideProps(context) {
    const providers = await getProviders();
    return {
        props:{
            providers
        }
    }
}
