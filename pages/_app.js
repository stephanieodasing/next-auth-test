import '../styles/globals.css'
import Layout from '../components/layout/layout';
import {Provider} from 'next-auth/client';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function MyApp({ Component, pageProps }) {
  return (
    <Provider session={pageProps.session}>
       <ToastContainer position="top-right" closeOnClick autoClose={2000} />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  )
}

export default MyApp;

