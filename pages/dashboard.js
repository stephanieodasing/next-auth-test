import { useSession, getSession } from "next-auth/client";
import { useRouter } from "next/router";
import {useEffect} from 'react'

// const router = useRouter();

export default function dashboard() {
const router = useRouter();
  const [session, loading] = useSession();

  if (!session) {
    //   return null

    useEffect(()=>{
        router.push("/")
    }, [])
  }

  return <h1>Dashboard</h1>;
}
