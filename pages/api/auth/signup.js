
import { hashPassword } from "../../../lib/auth";
import prisma from "../../../lib/prisma";
// import { PrismaClient } from '@prisma/client'

// const prisma = new PrismaClient();


async function handler(req, res) {
  if (req.method !== "POST") {
    return;
  }

  console.log("req.body", req.body);
  const data = req.body;
  const email = req.body.enteredEmail;
  const password = req.body.enteredPassword;
  const confirmPassword = req.body.enteredConfirmPassword;

  if (
    !email ||
    !email.includes("@") ||
    !password ||
    password.trim().length < 7 ||
    password !== confirmPassword
  ) {
    res.status(422).json({ message: "Invalid input" });
    return;
  }

  //   const client = await connectToDatabase();
  //   const db = client.db();

  //   const existingUser = await db.collection("users").findOne({ email: email });
  //   if (existingUser) {
  //     res.status(422).json({ message: "User exists already!" });
  //     client.close();
  //     return;
  //   }

  //   const hashedPassword = await hashPassword(password);

  //   const result = await db.collection("users").insertOne({
  //     email: email,
  //     password: hashedPassword,
  //   });

  //   res.status(201).json({ message: "Created user!" });
  //   client.close();

  const user = await prisma.users.findFirst({
    where: {
      email: email,
    },
  });

  if(user !== null){
    res.status(422).json({ message: "User exists already!" , status : "Error"});
  }else{
    const newUser = await prisma.users.create({
      data: {
        firstName: "FirstName", 
        lastName: "LastName", 
        email: email, 
        password: password,
      }
    })

    if(newUser){
      res.status(201).json({ message: "Created user!", status: "Success" });
    }
  }


}

export default handler;
