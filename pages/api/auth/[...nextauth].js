import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
// import { PrismaClient } from '@prisma/client'
import prisma from "../../../lib/prisma";
let userAccount = null;

// const prisma = new PrismaClient();

const configuration = {
    cookie: {
        secure: process.env.NODE_ENV && process.env.NODE_ENV === 'production',
    },
    session: {
        jwt: true,
        maxAge: 30 * 24 * 60 * 60
    },
    providers: [
        Providers.Facebook({
          clientId: process.env.FACEBOOK_CLIENT_ID,
          clientSecret: process.env.FACEBOOK_CLIENT_SECRET
        }),
        Providers.Google({
            id:"google",
            clientId: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
          }),
        Providers.Credentials({
            id: 'credentials',
            // name: "Login",
            async authorize(credentials) {
                const user = await prisma.users.findFirst({
                    where: {
                        email: credentials.email,
                        password: credentials.password
                    }
                });
                
                console.log(user)

                if (user !== null)
                {
                    userAccount = user;
                    console.log(user)
                    return user;
                }
                else {
                  console.log("no")
                    return null;
                }
            }
        })
    ],
    callbacks: {
        async signIn(user, account, profile) {
            if (typeof user.userId !== typeof undefined)
            {
                if (user.isActive === '1')
                {
                    return user;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        },
        async session(session, token) {
            if (userAccount !== null)
            {
                session.user = userAccount;
            }
            else if (typeof token.user !== typeof undefined && (typeof session.user === typeof undefined 
                || (typeof session.user !== typeof undefined && typeof session.user.userId === typeof undefined)))
            {
                session.user = token.user;
            }
            else if (typeof token !== typeof undefined)
            {
                session.token = token;
            }
            return session;
        },
        async jwt(token, user, account, profile, isNewUser) {
            if (typeof user !== typeof undefined)
            {
                token.user = user;
                console.log(token.user)
            }
            return token;
        }
    }
}
export default (req, res) => NextAuth(req, res, configuration)