import { useState, useRef, useEffect } from "react";
import { getProviders, useSession, getSession, signIn } from "next-auth/client";
import { useRouter } from "next/router";
import Image from "next/image";
import Link from "next/link";
import { toast } from "react-toastify";

function AuthForm(props) {
  const [session, loading] = useSession();
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  const confirmPasswordInputRef = useRef();
  const [isLogin, setIsLogin] = useState(true);
  const [error, setError] = useState("");
  const router = useRouter();
  const [subsList, setSubsList] = useState([]);

  async function submitHandler(e) {
    e.preventDefault();
    const enteredEmail = emailInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;
    const enteredConfirmPassword = confirmPasswordInputRef.current.value;

    const response = await fetch("/api/auth/signup", {
      method: "POST",
      body: JSON.stringify({
        enteredEmail,
        enteredPassword,
        enteredConfirmPassword,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await response.json();
    if (!response.ok) {
      console.log(data);
    }
    console.log("data", data);

    if (data.message !== null) {
      // setError(data.message)
      if (data.status == 'Success') {
        toast.success(data.message);
      }else{
        toast.error(data.message);
      }

      
    }
    if (response.ok) {
      router.replace("/signin");
    }
  }


  useEffect(async () => {
    console.log('test');
    if (loading) {
      const { data } = await fetch('/api/auth/getGoogleData', {
        withCredentials: true,
      });
      console.log(data);
      setSubsList(
        data.map((sub) => ({
          id: sub.id,
          title: sub.snippet.title,
        }))
      );
      setLoading(false);
    }
  }, [loading]);

  return (
    <div className="">
      <section>
        <form onSubmit={submitHandler}>
          {error ? <div>{error}</div> : null}
          <div>
            <label htmlFor="email" className="text-sm font-normal">
              Email
            </label>
            <input type="email" id="email" required ref={emailInputRef} />
          </div>
          <div>
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              required
              ref={passwordInputRef}
            />
          </div>
          <div>
            <label htmlFor="confirmPassword">Confirm Password</label>
            <input
              type="password"
              id="confirmPassword"
              required
              ref={confirmPasswordInputRef}
            />
          </div>
          <div>
            <button>SIGNUP</button>
            <button onClick={() => signIn()}>Sign In Google</button>


            <Link href="/signin">
              <a>
                Already have an account? <strong>LOG IN</strong>
              </a>
            </Link>
          </div>
        </form>
      </section>
    </div>
  );
}

export async function getServerSideProps(context) {
  const providers = await getProviders();
  console.log("providers", providers);
  return {
    props: {
      providers,
    },
  };
}

export default AuthForm;
